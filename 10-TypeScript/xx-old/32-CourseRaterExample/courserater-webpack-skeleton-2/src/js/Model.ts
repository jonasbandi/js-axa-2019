import {Array} from 'core-js';

export interface IRating {
    id: string;
    name: string,
    grade: number
}

export class Model {
    public ratings:  Array<IRating> = [];
    public newRating: IRating = this.createNewRating();

    public addRating(): void {
        this.ratings.push(this.newRating);
        this.newRating = this.createNewRating();
        this.notifyChange();
    }

    public removeRating(ratingId) {
        var index = this.ratings.findIndex(function (r) {
            return r.id === ratingId;
        });
        this.ratings.splice(index, 1);
        this.notifyChange();
    }

    private createNewRating(): IRating {
        const id = getId();
        return {id, name: '', grade: undefined};
    }

    private notifyChange() {
        $(this).trigger('change');
    }
}


function getId(): string {
    return '' + Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
}
