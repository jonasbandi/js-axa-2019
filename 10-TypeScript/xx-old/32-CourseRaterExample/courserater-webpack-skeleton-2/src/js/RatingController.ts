import {Model, IRating} from './Model'
import {NewRatingView} from './NewRatingView'
import {RatingListView} from './RatingListView'

export class RatingController {

    constructor(private model: Model, private newRatingView: NewRatingView,
                private ratingListView: RatingListView) {

        $(this.newRatingView).on('ratingChanged', (event, rating: IRating) => this.updateRating(rating));
        $(this.newRatingView).on('ratingAdded', (event, rating: IRating) => this.addRating());
        $(this.ratingListView).on('ratingRemoved', (event, ratingId: string) => {
            this.removeRating(ratingId)
        });

        $(this.model).on('change', () => this.renderViews());

        this.renderViews();
    }

    renderViews() {
        this.newRatingView.render(this.model.newRating);
        this.ratingListView.render(this.model.ratings);
    }

    updateRating(rating: IRating) {
        this.model.newRating.name = rating.name;
        this.model.newRating.grade = rating.grade;
    }

    addRating() {
        this.model.addRating();
    }

    removeRating(ratingId: string) {
        this.model.removeRating(ratingId);
    }

}
