const ts = require("typescript");

function inMemoryLanguageService(files) {
    function getFile(filename) {
        return ts.forEach(files, f => f.filename === filename ? f : undefined);
    }
    var host = {
        getScriptFileNames: () => files.map(f => f.filename),
        getScriptVersion: filename => {
            let file = getFile(filename);
            let version = file ? file.version : 0;
            return version;
        },
        getScriptSnapshot: filename => {
            console.log('Get snapshot:', filename);
            let file = getFile(filename);
            let text = file ? file.text : '';
            return ts.ScriptSnapshot.fromString(text);
        },
        // TODO do something else with the log messages
        log: message => undefined, //console.log('log: ' + message),
        trace: message => undefined, //console.log('trace: ' + message),
        error: message => undefined, //console.log('error: ' + message),
        getCurrentDirectory: () => '',
        getScriptIsOpen: () => true,
        getDefaultLibFileName: () => "lib.d.ts",
        getCompilationSettings: () => { return {}; },
    };
    return ts.createLanguageService(host, ts.createDocumentRegistry())
}

var f = {
    filename: "triangle.ts", version: 0,
    text: "class Triangle { edges() { return 3; }}" 
    // text: "let x:string = 42"
};

var langSvc = inMemoryLanguageService([f]);

// var textChanges = langSvc.getFormattingEditsForDocument(f.filename, defaultFormatCodeOptions());
// console.log('number of text changes: ' + textChanges.length);

var output = langSvc.getEmitOutput("triangle.ts").outputFiles[0].text;
console.log(output);
var output = langSvc.getSyntacticDiagnostics("triangle.ts");
console.log(output);
var output = langSvc.getSemanticDiagnostics("triangle.ts");
console.log(output);


function defaultFormatCodeOptions() {
    return {
        IndentSize: 4,
        TabSize: 4,
        NewLineCharacter: ts.sys.newLine,
        ConvertTabsToSpaces: true,
        InsertSpaceAfterCommaDelimiter: true,
        InsertSpaceAfterSemicolonInForStatements: true,
        InsertSpaceBeforeAndAfterBinaryOperators: true,
        InsertSpaceAfterKeywordsInControlFlowStatements: true,
        InsertSpaceAfterFunctionKeywordForAnonymousFunctions: false,
        InsertSpaceAfterOpeningAndBeforeClosingNonemptyParenthesis: false,
        PlaceOpenBraceOnNewLineForFunctions: false,
        PlaceOpenBraceOnNewLineForControlBlocks: false,
    };
}