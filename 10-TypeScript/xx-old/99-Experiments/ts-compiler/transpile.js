const ts = require("typescript");

const source = `

let x: string  = 'test'
x = 42;

`;

let result = ts.transpileModule(source, { reportDiagnostics: true, compilerOptions: { module: ts.ModuleKind.CommonJS} });

console.log(JSON.stringify(result));