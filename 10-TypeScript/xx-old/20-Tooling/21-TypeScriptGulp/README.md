Install typescript globally:
    
    npm install -g typescript 
    
   
Compile the es6 file to es5 with typescript:

    tsc src/app.ts --out build/app.js


More options:

    tsc src/app.ts --watch --out build/app.js --sourceMap