module.exports = {
    devtool: 'source-map',
    entry: "./app/app.ts",
    output: {
        path: './public',
        filename: "bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            {test: /\.ts$/, exclude: /node_modules/, loaders: ['ng-annotate', 'ts']},
            //{test: /\.ts$/, loader: 'ts', exclude: /node_modules/},
            {test: /\.html$/, loader: 'raw', exclude: /node_modules/}
        ]
    }
};