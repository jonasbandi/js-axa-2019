export class CartService {
    static ID = 'CartService';

    public products = [];

    constructor(){
    }

    addProduct(product){
        this.products.push(product);
    }

}
