import {CartController} from './CartController'
import {CartService} from './CartService'

export function init (module : ng.IModule){
    module.config(($stateProvider) => {
        $stateProvider.state("cart", {
            url: "/cart",
            template: require('./pd-cart.html'),
            controller: CartController,
            controllerAs: 'cart'
        });
    });

    module.service(CartService.ID, CartService)
}

