export class CatalogController {
    private catalogItems;
    private cartService;

    /*@ngInject*/
    constructor($http, CartService) {

        this.cartService = CartService;

        $http.get('http://localhost:3000/products')
            .then((result) => {
                console.log('Got catalog items: ' + result)
                this.catalogItems = result.data
            });
    }

    addProduct(product){
        this.cartService.addProduct(product);
    }
}