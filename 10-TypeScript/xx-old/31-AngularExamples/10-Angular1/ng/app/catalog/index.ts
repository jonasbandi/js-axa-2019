import {CatalogController} from './CatalogController'

export function init (module : ng.IModule){
    module.config(($stateProvider) => {
        $stateProvider.state("catalog", {
            url: "/",
            template: require('./pd-catalog.html'),
            controller: CatalogController,
            controllerAs: 'catalog'
        });
    })
}



