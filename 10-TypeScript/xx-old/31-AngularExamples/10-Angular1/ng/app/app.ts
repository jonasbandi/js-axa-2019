import './webpack'
import * as angular from 'angular';
import * as uirouter from 'angular-ui-router';
import * as catalog from './catalog/index';
import * as cart from './cart/index';

const pdshop = angular.module('pd-shop', [uirouter]);

catalog.init(pdshop);
cart.init(pdshop);

pdshop.config(($stateProvider, $locationProvider, $urlRouterProvider) => {
    $urlRouterProvider.otherwise("/");
});

console.log('Hello from TS & Angular');

pdshop.directive('pdShop', pdShop);


function pdShop() : ng.IDirective{
    return {
        template: require('./pd-shop.html')
    }
}

