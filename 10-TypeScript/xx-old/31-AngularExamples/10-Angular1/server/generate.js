module.exports = function(){
	var faker = require('faker');
	var _ = require('lodash');
	
	return {
		products: _.times(100, function(n){
			return {
				id: n,
				name: faker.commerce.productName(),
				category: faker.commerce.product(),
				price: faker.finance.amount(),
				url: faker.image.food() + '/' + n % 10
			}
		})
	}
}