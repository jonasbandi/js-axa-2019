// function delayAsync(timeout: number): Promise<void> {
//   return new Promise<void>((resolve) => setTimeout(resolve, timeout));
// }
// 
// async function testAsync(): Promise<void> {
//   console.log('Awaiting ...');
//   await delayAsync(1000);
//   console.log('Finished awaiting!');
//   throw new Error('Oh snap!');
// }
// 
// testAsync().catch(err => console.log(err.stack || err));