class Controller {
    private count: number = 0;

    increment(){
        this.count++;
        alert(this.count);
    }
}

let controller = new Controller();

document.getElementById('btn').addEventListener('click', controller.increment);