export interface IRating {
    id: number;
    name: string,
    grade: number
}

export class Model {
    public ratings:  Array<IRating> = [];
    public newRating: IRating = this.createNewRating();

    public addRating(): void {
        this.ratings.push(this.newRating);
        this.newRating = this.createNewRating();
        this.notifyChange();
    }

    public removeRating(ratingId: number): void {
        var index = this.ratings.findIndex(r => r.id === ratingId);
        if (index >= 0) {
            this.ratings.splice(index, 1);
            this.notifyChange();
        }
    }

    private createNewRating(): IRating {
        const id: number = getId();
        return {id, name: '', grade: undefined};
    }

    private notifyChange(): void {
        $(this).trigger('change');
    }
}


function getId(): number {
    return Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
}
