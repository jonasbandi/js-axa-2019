(function(global) {
    'use strict';

    var controller = {
        init : function() {

            var model = global.courserater.model;
            var statisticsView = new global.courserater.StatisticsView();

            $(model).on('change', renderViews);

            renderViews(); // initial rendering


            function renderViews() {
                statisticsView.render(model.ratings);
            }
        }
    };

    global.courserater = global.courserater || {};
    global.courserater.statisticsController = controller;

})(window);
