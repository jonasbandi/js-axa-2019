import {model} from './Model'
import {NewRatingView} from './NewRatingView'
import {RatingListView} from './RatingListView'

export var ratingController = {
    init : function() {

        var newRatingView = new NewRatingView();
        var ratingListView = new RatingListView();

        $(newRatingView).on('ratingChanged', updateRating);
        $(newRatingView).on('ratingAdded', addRating);
        $(ratingListView).on('ratingRemoved', removeRating);
        $(model).on('change', renderViews);

        renderViews(); // initial rendering


        function renderViews() {
            newRatingView.render(model.newRating);
            ratingListView.render(model.ratings);
        }

        function updateRating(event, rating) {
            model.newRating.name = rating.name;
            model.newRating.grade = rating.grade;
        }

        function addRating() {
            model.addRating();
        }

        function removeRating(event) {
            model.removeRating(event.ratingId);
        }

    }
};
