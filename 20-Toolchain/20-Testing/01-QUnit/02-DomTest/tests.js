var calc;
module('Calculator', {

	setup: function() {
        calc = new Calculator($("#div1"));
	},
	teardown: function() {
	}
});

test('add method', function() {
    calc.add(1, 2);
	strictEqual($(".calc-result").text(), "3");
});
