describe('Bowling Game', function(){
    var game;
    beforeEach(function(){
        game = new BowlingGame();
    });

    it('should calculate the score for a gutter game', function(){
        for (var i = 0; i < 20; i++){
            game.roll(0);
        }
        expect(game.getScore()).toEqual(0);
    });
})