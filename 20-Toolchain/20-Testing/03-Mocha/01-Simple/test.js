
suite('Calculator', function() {

    var calc;

	before(function() {
		console.log('before');
	});
	
	after(function() {
		console.log('after');
	});
	
	setup(function() {
		console.log('setup');
        calc = new Calculator();
	});
	
	teardown(function() {
		console.log('teardown');
	});
	
	test('add method', function() {
		console.log('add method');
        expect(calc.add(1,1)).to.equal(2);
	});
	
	suite('inner suite', function() {
		setup(function() {
			console.log('inner setup');
		});		
		
		teardown(function() {
			console.log('inner teardown');
		});
		test('this is my second test', function() {
			console.log('second');
			expect(1).to.equal(1);
		});
		
	});

});