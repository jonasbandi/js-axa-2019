You can initialize and run a basic Jasmine example with the following commands:

	npm init
	npm i -D jasmine
	jasmine init
	jasmine examples
	jasmine
