beforeEach(function () {
  jasmine.addMatchers({
    toBeBetween: function() {
      return {
        compare: function(actual, lowerBound, upperBound) {

          return {
            pass: actual >= lowerBound && actual <= upperBound
          }
        }
      };
    }
  });
});
