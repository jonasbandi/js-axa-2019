## Linting und Minification

Versuchen Sie die Demos aus der Vorlesung selbst nachzuvollziehen.  
Verwenden Sie dazu das Meterial in den Direcotries `02-Linting` und `03-Packaging`.
Verwenden Sie die online version von JSLint/JSLint und auch die entsprechenden commandline tools.

Versuchen Sie ihren JavaScript Code aus den vergangenen Vorlesungen (z.B. eine Version des Course-Raters) zu linten und zu minifizieren.


<br/>
## NPM & Bower
Analog der Demo: Erstellen sie ein neues Projekt mit npm und Bower. Verwenden Sie npm um Grunt und JSHint als Build-Dependencies zu integrieren und Bower um jQuery als Web-Asset zu integrieren.

Führen Sie dazu die folgenden Schritte aus und versuchen Sie diese zu verstehen:

- `npm install -g bower grunt-cli` oder `npm install -g bower gulp`
- `npm init`
- `npm install grunt grunt-contrib-jshint --save-dev` oder `npm install gulp gulp-jshint --save-dev`
- `bower init`
- `bower install jquery --save`
- Erstellen Sie ein `index.html` und ein `app.js`. Binden Sie `app.js` in die html-Seite ein und manipulieren Sie das DOM z.B. `$('body').html('Hello from jQuery)'`
- Erstellen sie ein `gruntfile.js` oder ein `gulpfile.js` welches auf den JavaScript sources des Projektes ein Linting durchführt (siehe dazu auch die Slides).
- Führen Sie den Build aus mit `grunt`.
- (Fakultativ) Binden sie nun auch JSCS als zusätzlichen statischen Code-Analyzer ein.
- Binden sie ein zweites JavaScript file ein (`module.js`), das z.B. ein weiteres Element an dem Body anhängt. Erweitern Sie den Build, so dass `app.js` und `module.js` konkateniert und minifiziert werden. Benutzen Sie z.B. uglify.js resp. ein passendes Grunt/Gulp Plugin dazu.


<br/>
## Yeoman
Verwenden Sie Yeoman um ein Angular Applikation zu generieren. 

Führen Sie dazu die folgenden Schritte aus und versuchen Sie diese zu verstehen:

```
npm install -g yo
npm install -g generator-angular
yo angular
grunt
grunt serve
grunt watch
```

<br/>
## Gulp vs. Grunt
Vergleichen Sie das Beispiel in `01-Intro/02-SimpisticToDo-Gulp` mit dem Demo Beispiel `01-Intro/02-SimpisticToDo-Grunt`.

Installieren Sie die nötigen Dependencies mit `npm install` und `bower install`.

Führen Sie den Build aus mit `grunt` resp `gulp`. Untersuchen Sie das Ergebnis.

Untersuchen Sie die beiden Build files `gruntfile.js` resp. `gulpfile.js`. Welches ist für Sie verständlicher? Welche Vorteile/Nachteile sehen Sie?


<br/>
## Testing

Versuchen Sie die Demos aus der Vorlesung selbst nachzuvollziehen.  
Verwenden Sie dazu das Meterial in dem Direcotry `04-Testing`.

Wählen Sie eines der vorgestellten Testing-Frameworks und versuchen Sie ihren ihren JavaScript Code aus den vergangenen Vorlesungen zu testen.


<br/>
## Angular Testing
- Schreiben Sie im Projekt `30-UnitTest/04-CourseRater` einen weiteren Unit-Test, welcher den `ratingsController` testet.
- Schreiben Sie im Projekt `31-IntegrationTest/02-CourseRater` einen weiteren Integration-Test, welcher das Hinzufügen eines Ratings testet.
- Nehmen Sie das Course-Rater Projekt in welchem Sie das Backend angebunden haben und ersetzen Sie das Backend mit einem Mock-Backend, welches mit ngMockE2E/$httpBackend definiert ist.


<br/>
### Java Toolchain Integration

Untersuchen Sie das Beispielprojekt, welches eine mögliche Integration eines JavaScript-Builds mit Maven in ein JSF-Projekt demonstriert:
- Wechseln Sie in das Verzeichnis `04-Toolchain/02-Java/courserater`
- Führen Sie den Build aus: `mvn package`
- Deployen Sie das WAR in den Application Server. z.B. `cp target/courserater-jsf.war $JBOSS_HOME/standalone/deployments` 
- Öffnen Sie die URL `http://localhost:8080/courserater-jsf` im Browser
- Inspizieren Sie welche JavaScript Files geladen werden
- Öffnen Sie die URL `http://localhost:8080/courserater-jsf/jsmodule/home.jsf?jsDebug=1` im Browser
- Inspizieren Sie welche JavaScript Files geladen werden
- Inspizieren Sie das `pom.xml`. Wie wird die JavaScript Concatenation und Minification in den Maven Build eingebunden?
- Studieren Sie die Dokumentation der beiden verwendeten Maven Plugins:
	- http://samaxes.github.io/minify-maven-plugin/ 
	- https://github.com/alexo/wro4j
- (Advanced) Importieren Sie das Projekt in Ihre IDE und veruchen Sie ein "nahtloses" Debugging einzurichten.
- (Advanced) Versuchen Sie einen Lösung mit Browserify oder RequireJS (siehe Modularisierung) in den Maven Build einzubauen.
