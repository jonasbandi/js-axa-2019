var math = (function() {

    // the add method - very complicated
    add = function(x, y) {
        return x + y;
    },

    // the subtract method - also complicated
    subtract = function(x, y) {
        return x - y;
    },

    // the multiply method - even more complicated
    multiply = function(x, y) {
        return x * y;
    },

    // the divide method - I don't understand it, but it works!
    divide = function(x, y) {
        if (y === 0) {
            alert("Can't divide by 0!");
            return 0;
        }
        return x / y;
    }

    return {
        add: add,
        subtract: subtract,
        multiply: multiply,
        divide: divide
    };

})();