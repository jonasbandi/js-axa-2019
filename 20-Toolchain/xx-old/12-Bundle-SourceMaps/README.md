Generate the minified bundle with source map:

    npm i -g uglifyjs
    cd calculator/src
    uglifyjs math.js calculator.js -m -c -o app.min.js --source-map app.min.map

Open `index.html` in a modern browser and check that the minified bundle is loaded. 
Still, you can set a breakpoint in `math.js`... 