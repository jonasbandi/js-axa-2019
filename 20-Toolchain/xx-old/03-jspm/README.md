Initializing project:

    npm install jspm -g
    npm install jspm --save-dev
    jspm init
    jspm install jquery
    
    
Running project:

    npm install
    jspm install
    npm start
    

Bundle for production:

    jspm bundle app --inject

Or create a complete bundle:

    jspm bundle-sfx app
