module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'build/bundle.js'
    },
    devtool: 'source-map'
};