module.exports = function(grunt) {

    grunt.initConfig({

        jshint: {
            options: {
//                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'app.js',
                'greeter.js'
            ]
        },

        qunit: {
            all: ['tests.html']
        },

        watch: {
            src: {
                files: ['Gruntfile.js', '*.js', '*.html' ],
                tasks: ['jshint', 'qunit', 'notify:watch']
            }
        },
        notify: {
            task_name: {
                options: {
                    // Task-specific options go here.
                }
            },
            watch: {
                options: {
                    title: 'Task Complete',  // optional
                    message: 'Done' //required
                }
            },
            server: {
                options: {
                    message: 'Server is ready!'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    grunt.registerTask('default', [ 'jshint']);
};
