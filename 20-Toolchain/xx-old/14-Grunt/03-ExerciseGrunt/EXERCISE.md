## JavasScript Build mit Node.js und Grunt


Dieser Block thematisiert eine JavaScript-Toolchain mit Grunt. Grunt ist ein Build-Tool vergleichbar mit Ant.

Nach der Durcharbeitung der Beispiele haben Sie ein Verständnis zu folgenden Themen gewonnen:

- Grunt Tasks ausführen
- Aufbau eines Gruntfiles
- Kenntnisse zu typischen Teilschritten eines JavaScript-Builds
- Unit-Tests ausführen
- Eine beliebige Taskkette bei jedem Speichern ausführen
- live-reload im Browser
- Einrichten einer komfortablen, flexiblen, automatisierten Entwicklungsumgebung basierend auf Grunt, Browser und Texteditor (oder IDE) nach Wahl


Wechseln Sie in das Verzeichnis `07-Grunt/03-BuildExample`.  
Installieren Sie alle nötigen Node Packages mit: `npm install`

Das vorliegende Gruntfile `Gruntfile.js` bietet Tasks für folgende Aufgaben:

- `copy`: Kopiert die Webdateien ins Distributions-Verzeichnis.
- `concat`: Setzt JavaScript und CSS-Dateien zu jeweils eine einzigen Datei zusammen
- `uglify`: minimiert JavaScript Dateien
- `cssmin`: minimiert CSS-Dateien
- `htmlcompressor`: minimiert HTML-Dateien
- `jshint`: prüft Syntax und Stil der JavaScript-Dateien
- `watch`: beobachtet Dateien und führt Grunt-Tasks aus sobald die Dateien geändert werden
- `karma`: startet automatisierte Test
- `server`: startet die Application und lädt sie im Browser; automatischer Browser-Reload beim Speichern


##### Task 1: Grunt ausführen
- Grunt Task ‘clean’ ausführen: `grunt clean`
- Grunt default Task ausführen: `grunt`

Grunt führt den Default-Task aus und erzeugt die Applikation im Verzeichnis `build`. Inspizieren Sie den Output der Console:

- Welche sechs Tasks wurden ausgeführt und in welche Reihenfolge?
- Rufen Sie die Tasks clean, copy und concat einzeln nacheinander auf
- Inspizieren Sie Gruntfile.js und finden Sie heraus, wo z.B. der concat Task definiert ist und welche Dateien - der concat Task zusammensetzt.
- Inspizieren Sie Gruntfile.js und finden Sie heraus, wie die default-Taskkette definiert wurde.
- Inspizieren Sie die erzeugten Artefakte im Verzeichnis ‘build’.
- Advanced: Was ist ein grosser Unterschied in diesem Beispiel zu der Build-Chain dem Beispiel in `07-Grunt/02-SimplisticTodo`? Hint: Versuchen die die Applikation zu Debuggen ... schauen sie sich den `usemin` task im `02-SimplisticTodo` an (https://github.com/yeoman/grunt-usemin).

##### Task 2: ‘karma’ Task benutzen
- Grunt-Task ‘karma’ ausführen: `grunt karma`
- Inspizieren Sie den Output von Grunt
- 4 Tests sollten erfolgreich ausgeführt worden sein.

- Ändern Sie den Test  in `test/observableSpec.js` und lassen sie den Test fehlschlagen. (z.B. Zeile 23: ‘done’ -> ‘not done’)
- Inspizieren Sie den Output nach einem erneuten `grunt karma`

##### Task 3: Den Task ‘karma’ bei Änderungen automatisiert aufrufen

Passen Sie den Task ‘watch’ in `Gruntfile.js` folgendermassen an:

- Erweitern Sie die bestehende Konfiguration der zu beobachteten Dateien des `watch` Tasks um `test/**/*.js`
- Erweitern Sie die bestehende Konfiguration der auszuführenden Tasks des `watch` Tasks um den `karma` Task 
- Starten sie Grunt mit `grunt watch`
- Ändern Sie den Test `test/observableSpec.js` und lassen Sie den Test fehlschlagen
- karma führt alle Tests nach dem Speichern automatisiert aus
- Richten Sie den Desktop so ein, dass sie während dem Entwickeln ständig die Console und den Editor sehen können und experimentieren Sie weiter. 


##### Task 4: ‘server’ Task benutzen
- Grunt-Task ‘server’ aufrufen: `grunt server`
- Grunt startet einen Browser und zeigt die Website unseres Projektes an
- Öffnen Sie in einem Texteditor die Datei `public_html/index.html` und ändern Sie auf Zeile 30 den Namen “Diego Maradona” zu einem anderen Namen (z.B. ihren eigenen).
- Speichern Sie die Datei und beobachten Sie den Browser. Nach dem Speichern wird automatisch die ganze Applikation neu gebaut und im Browser neu geladen. Der angepasste Name erscheint.

Die Grunt-Tasks, welche beim Speichern ausgeführt werden, lassen sich über das ‘tasks’-Property des ‘regarde’ Task (franz. für watch) definieren.


##### Task 5: ‘server’ Task anpassen

- Passen Sie den `regarde` Task so an, dass auch der `karma` Task ausgeführt wird
- Ändern Sie `public_html/index.html` und speichern Sie die Datei.
Inspizieren Sie den Output
- Richten Sie den Desktop ideal ein: An einem Arbeitplatz mit zwei Bildschirmen kann man den Editor und die Console auf den einen Bildschirm, den Browser mit den WebDevTools auf den anderen Bildschirm platzieren. Das ergibt eine einzigartige Entwicklungsumgebung für HTML-Client-Applikationen: Bei jedem Speichern werden die Tests automatisch ausgeführt und die Applikation im Browser neu geladen. Test-Feedback und Browser-Update as-you-type! Zudem kann jeder Entwickler seinen Lieblings-Editor verwenden.

##### Task 6 (Advanced): CI Integration
- Überlegen Sie sich welche Taskkette Sie verwenden würden, um sie auf Jenkins auszuführen.
- Blog zu CI-Integration von JavaScript-Builds: http://www.zealake.com/2012/12/25/run-all-your-javascript-qunit-tests-on-every-commit/
- Output in JUnit-XMl-Format: https://github.com/johnbender/grunt-junit

