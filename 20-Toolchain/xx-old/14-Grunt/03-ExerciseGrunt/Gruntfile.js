/*global module:false, require: false */
module.exports = function(grunt) {

	var DEFAULT_TASKS = ["jshint", "copy", "concat", "cssmin", "htmlcompressor", "uglify"],
		WATCH_FILES = ['Gruntfile.js', 'public_html/js/**/*.js','public_html/**/*.html','public_html/**/*.css'],
		DOC_ROOT = "public_html/",
		DIST_DIR = "build/";
		
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			dist: {
				files: [
					{
						expand: true,
						src: [DOC_ROOT + '**'],
						dest: DIST_DIR
					}
				]
			}
		},
		concat: {
			options: {
				separator: ''
			},
			js: {
				src: [
					DOC_ROOT + 'js/observable.js',
					DOC_ROOT + 'js/todo-service.js',
					DOC_ROOT + 'js/application-controller.js',
					DOC_ROOT + 'js/main.js'
				],
				dest: DIST_DIR + 'public_html/js/all-concat.js'
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: DIST_DIR + DOC_ROOT + "js/all-concat.js",
				dest: DIST_DIR + DOC_ROOT + "js/<%= pkg.name %>.js"
			}
		},
		cssmin: {
			combine: {
				files: {
					'build/public_html/css/<%= pkg.name %>.css':
						[ DOC_ROOT + 'css/layout.css',
						  DOC_ROOT + 'css/components.css',
						  DOC_ROOT + "css/animations.css" ]
				}
			}
		},
		htmlcompressor: {
			compile: {
				files: {
					'build/public_html/index.html': DOC_ROOT + 'index.html'
				},
				options: {
					type: 'html',
					preserveServerScript: true
				}
			}
		},
		clean: ["build"],
		jshint: {
			options: {
				/*** Enforcing options ***/
				// Set these to `true` to enforce, or `false` to relax.
				"bitwise":      true,
				"curly":        true,
				"eqeqeq":       false,
				"forin":        false,
				"immed":        true,
				"latedef":      false,
				"newcap":       true,
				"noarg":        true,
				"noempty":      true,
				"nonew":        true,
				"plusplus":     false,
				"regexp":       false,
				"undef":        true,
				"strict":       false,
				"trailing":     true,

				/*** Relaxing options ***/
				// Set these to `true` to relax, or `false` to enforce.
				"asi":          false,
				"boss":         true,
				"debug":        false,
				"eqnull":       true,
				"esnext":       true,
				"evil":         true,
				"expr":         true,
				"funcscope":    false,
				"globalstrict": false,
				"iterator":     false,
				"lastsemic":    false,
				"laxbreak":     false,
				"laxcomma":     false,
				"loopfunc":     true,
				"multistr":     false,
				"onecase":      false,
				"proto":        false,
				"regexdash":    false,
				"scripturl":    false,
				"shadow":       true,
				"smarttabs":    true,
				"sub":          false,
				"supernew":     false,
				"validthis":    true,

				/*** Environments ***/
				// Set each environment that you're using to `true`.
				"browser":      true,
				"couch":        false,
				"devel":        false,
				"dojo":         false,
				"jquery":       true,
				"mootools":     false,
				"node":         false,
				"nonstandard":  false,
				"prototypejs":  false,
				"rhino":        false,
				"wsh":          false,

				/*** Legacy from JSLint ***/
				// Set these to `true` to enforce, or `false` to relax.
				"nomen":        false,
				"onevar":       false,
				"passfail":     false,
				"white":        false
			},
			all: ['Gruntfile.js', DOC_ROOT + 'js/*.js']
		},
		karma: {
			unit: {
				configFile: 'karma.conf.js'
			}
		},

		connect: {
			options: {
				port: 9000,
				livereload: 35729 // the connect middleware injects the livereload script into the served html page
			},
			all: {
				options: {
					open: true,
					base: DIST_DIR + DOC_ROOT
				}
			}
		},

		open: {
			all: {
				// Gets the port from the connect configuration
				path: 'http://localhost:<%= connect.options.port%>/index.html'
			}
		},

		watch: {
			files: WATCH_FILES,
			tasks: DEFAULT_TASKS,
			options: { livereload: true } // trigger the livereload server, which is started by the connect task
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-htmlcompressor');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-open');

	// Default task(s).
	grunt.registerTask("default", DEFAULT_TASKS);
	grunt.registerTask("server", ["default", "connect", "open", "watch"]);
};