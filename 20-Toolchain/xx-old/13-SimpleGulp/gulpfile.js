var gulp = require('gulp');
var jshint = require('gulp-jshint');

gulp.task('analyze', function(){
    console.log('Analyzing');

    return gulp.src('./*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', {verbose: true}))
        .pipe(jshint.reporter('fail'));
});

gulp.task('default', ['analyze']);
