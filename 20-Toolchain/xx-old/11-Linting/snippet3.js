/*global doSomething*/
/*exported doSomethingWithNodes*/
/*jshint strict: true*/

function doSomethingWithNodes(nodes){
  /*jshint validthis: true*/
  'use strict';
  this.doSomething();

  for (var i = 0; i < nodes.length; ++i){
		if (i === 1){
			this.doSomethingElse(nodes[i]);
		}
  }

  doSomething();
}
