function doSomethingWithNodes(nodes){
  this.doSomething();

  for (var i = 0; i < nodes.length; ++i){
  	if (i == 1)
    	this.doSomethingElse(nodes[i]);
  }

  doSomething(); 
}