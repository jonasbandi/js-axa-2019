/*global doSomething */
/*jslint white:true */
/*jslint for:true */
/*jslint this:true */
function doSomethingWithNodes(nodes) {
    'use strict';
    var i;
    this.doSomething();

    for (i = 0; i < nodes.length; i = i + 1) {
        if (i === 1) {
            this.doSomethingElse(nodes[i]);
        }
    }

    doSomething();
}
