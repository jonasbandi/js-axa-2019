
### Packaging

Link: http://marijnhaverbeke.nl//uglifyjs

Order of files!

uglifyjs math.js calculator.js -c -m -o app.min.js 


Show Link:  
https://github.com/mishoo/UglifyJS2

Show options!


## Sourcemaps

Create your own source map:  
`uglifyjs math.js calculator.js -m -c -o app.min.js --source-map app.min.map`

Show debugging in browser.  


### Second example
`01-Intro/02-SimplisticToDo-Gulp`:

- change event registration to jQuery: `$('#addBtn').on('click', addText);`
- set breakpoint and step into -> unminified jQuery

### Browser behavior  
Start local webserver and show requests of browser.  
Show when the browser is loading the files (Chrome and FF behave differently)


### Problems with source maps: 
-> jQuery removed source map comment in 1.11/2.1

- Renaming of files
- No "manual" pairing in the browser
- Confusion 404 (Chrome has Source Maps enabled as default)
- mixing local minified with CDN map/unminified)
-> jQuery removed source map comment in 1.11