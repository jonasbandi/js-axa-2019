
## Linting

Show website:  
http://www.jslint.com/

Snippet:

```
function doSomethingWithNodes(nodes){
  this.doSomething();

  for (var i = 0; i < nodes.length; ++i){
  	if (i == 1)
    	this.doSomethingElse(nodes[i]);
  }

  doSomething(); 
}
```

Refactored for JSLint with *directives*:

```
/*global doSomething */
/*jslint indent: 2 */
function doSomethingWithNodes(nodes) {
  "use strict";
  var i;
  this.doSomething();

  for (i = 0; i < nodes.length; i = i + 1) {
    if (i === 1) {
      this.doSomethingElse(nodes[i]);
    }
  }

  doSomething();
}
```

Show website:
http://www.jshint.org/

Snippet refactored for JSHint:

```
/*global doSomething*/
/*exported doSomethingWithNodes*/
/*jshint strict: true*/
function doSomethingWithNodes(nodes){
  "use strict";
  this.doSomething();

  for (var i = 0; i < nodes.length; ++i){
		if (i == 1)
		this.doSomethingElse(nodes[i]);
  }

  doSomething(); 
}
```

`jshint snippet.js`  
`jshint -c jshintrc snippet.js`

Show options.  
Introduce target parameter instead of this.

Show IDE integration in webstorm.