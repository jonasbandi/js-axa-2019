## NPM

Link: npmjs.org

```
npm help npm search scaffold npm info underscore (versions, version)
npm install underscore npm uninstall underscore
npm list
npm update
npm init
```
```
--no-registry
npm prune
```

##### Versioning:
Link: https://www.npmjs.org/doc/misc/semver.html

##### Show Package Structure in node_modules:
```
cd node_modules/underscore
npm install
npm test
```

##### Registry:
/usr/local/lib/node_modules



## Bower

```
bower help
bower search
bower info
bower install
bower uninstall
bower list
bower update
bower init
bower cache
```
```
--save
--save-dev
--offline
```

.bowerrc
```
{
  "directory": "public/bower_components"
}
```


Bower install