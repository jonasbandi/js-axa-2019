import '../css/index.css'
import './base';
import './layout';
import $ from 'jquery'
import model from './model';

console.log('Starting... !!!!!!');

$(model).on('modelchange', () => {
    renderNewToDo();
    renderToDos();
});

const todoInput = $('#todo-text');
todoInput.on('keyup', updateCurrentToDo)

function updateCurrentToDo() {
    model.updateCurrentToDo(this.value)
}

function renderNewToDo() {
    todoInput.val(model.newToDo.title);
}

function renderToDos() {
    console.log('render');
    const $todoList = $('#todo-list');
    $todoList.html('');
    for (const toDo of model.toDoList) {
        $('<li>')
            .appendTo($todoList)
            .text(toDo.title)
            .append(
                $('<button>')
                    .text('X')
                    .on('click', () => removeToDo(toDo))
            );
    }
}

renderToDos();


const addToDo = () => {
    model.addToDo();
}

function removeToDo(toDo) {
    model.removeToDo(toDo)
}


var todoForm = document.getElementById('todo-form');
todoForm.addEventListener('submit', addToDo);