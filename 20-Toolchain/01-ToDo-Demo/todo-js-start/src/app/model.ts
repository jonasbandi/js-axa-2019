import $ from 'jquery'


class ToDo {
    title;

    constructor(title) {
        this.title = title;
    }
}

interface IModel {
    newToDo: ToDo,
    toDoList: ToDo[],
    addToDo: ()=> void,
    removeToDo: (ToDo) => void,
    updateCurrentToDo: (value: string) => void
}

const model: IModel = {
    newToDo: undefined,
    toDoList: undefined,
    addToDo: undefined,
    removeToDo: undefined,
    updateCurrentToDo: undefined
};


function addToDo() {
    model.toDoList.push(model.newToDo);
    model.newToDo = new ToDo('');
    console.log('added', model);
    notifyModelChange()
}

function removeToDo(toDo) {
    const index = model.toDoList.indexOf(toDo);
    model.toDoList.splice(index, 1);
    notifyModelChange();
}

function updateCurrentToDo(value: string) {
    model.newToDo.title = value;
    console.log('updating', model.newToDo);
}

function notifyModelChange() {
    $model.trigger('modelchange');
}

model.addToDo = addToDo;
model.removeToDo = removeToDo;
model.updateCurrentToDo = updateCurrentToDo;
model.newToDo = new ToDo('');
model.toDoList = [
    new ToDo('Learn JavaScript'),
    new ToDo('Learn React')
];
const $model = $(model);

export default model;