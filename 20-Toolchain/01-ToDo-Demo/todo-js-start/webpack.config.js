/* global module, require, __dirname*/
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {

  entry: {
    app:'./src/app/app.js',
    vendor: ['jquery', 'fittextjs']
  },
  output: {
    filename: './dist/[name].[chunkhash].js',
  },
  resolve: { extensions: ['.ts', '.js']},
  devtool: 'source-map',
  plugins: [
    new HtmlWebpackPlugin({template: 'src/index.html'}),
    new MiniCssExtractPlugin({filename: '[name].[chunkhash].css'})
  ],
  module: {
    rules: [
      { test: /\.ts$/, use: 'ts-loader', exclude: /node_modules/},
      { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/},
      {
        test: /\.css$/,
        use: [
          {loader: MiniCssExtractPlugin.loader}
          , 'css-loader'
        ]
      }
    ]
  },
  optimization:{
    namedModules: true,
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all'
        }
      }
    },
    noEmitOnErrors: true,
    concatenateModules: true
  },
};