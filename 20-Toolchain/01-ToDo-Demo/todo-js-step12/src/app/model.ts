import * as $ from "jquery";


class ToDo {
  title: string;

  constructor(title: string) {
    this.title = title;
  }
}


function createModel() {

  function notifyModelChange() {
    $model.trigger("modelchange");
  }

  function capitalize(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }

  function updateNewToDoModel(title) {
    if (model.newToDo.title !== title) {
      model.newToDo.title = capitalize(title);
      notifyModelChange();
    }
  }

  function addToDo() {
    model.toDoList.push(model.newToDo);
    model.newToDo = new ToDo("");

    notifyModelChange();
  }

  function removeToDo(toDo) {
    model.toDoList = model.toDoList.filter(t => t !== toDo);
    notifyModelChange();
  }

  const model = { updateNewToDoModel, addToDo, removeToDo, newToDo: undefined, toDoList: undefined };
  model.newToDo = new ToDo("");
  model.toDoList = [
    new ToDo('Learn JavaScript'),
    new ToDo('Learn React')
  ];
  const $model = $(model);

  return model;
}

export default createModel;
