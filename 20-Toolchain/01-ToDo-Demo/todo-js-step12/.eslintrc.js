module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "jquery": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended"
  ],
  "plugins": ["@typescript-eslint"],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module"
  },
  "rules": {
    "@typescript-eslint/indent": [
      "error",
      2
    ],
    "@typescript-eslint/explicit-member-accessibility": [
      0
    ],
    "@typescript-eslint/explicit-function-return-type": [
      0
    ],
    "@typescript-eslint/no-use-before-define": [
      0
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ]
  }
};
