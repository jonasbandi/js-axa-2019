import $ from 'jquery';
import 'fittextjs';

import '../css/index.css';

import './base';
import './layout';

const addToDo = () => {
  const input = $('#todo-text');
  const textValue = input.val();
  if (textValue) {
    $('<li>')
      .appendTo('#todo-list')
      .text(textValue)
      .append(
        $('<button>')
          .text('X')
          .on('click', removeToDo)
      );
    input.val('');
  }
};

function removeToDo(){
  const item = $(this).closest('li');
  item.remove();
  test();
}

function test(){
  return Promise.resolve().finally();
}

const todoForm = document.getElementById('todo-form');
todoForm.addEventListener('submit', addToDo);
