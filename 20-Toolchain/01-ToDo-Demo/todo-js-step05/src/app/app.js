import $ from 'jquery';
import 'fittextjs';

import './base';
import './layout';

var todoForm = document.getElementById('todo-form');

todoForm.addEventListener('submit', addToDo);

function addToDo(){
  var input = $('#todo-text');
  var textValue = input.val();
  if (textValue) {
    $('<li>')
      .appendTo('#todo-list')
      .text(textValue)
      .append(
        $('<button>')
          .text('X')
          .on('click', removeToDo)
      );
    input.val('');
  }
}

function removeToDo(){
  var item = $(this).closest('li');
  item.remove();
}
