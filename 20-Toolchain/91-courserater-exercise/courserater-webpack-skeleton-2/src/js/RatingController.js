import {Model, IRating} from './Model'
import {NewRatingView} from './NewRatingView'
import {RatingListView} from './RatingListView'

export class RatingController {

    constructor(model, newRatingView, ratingListView) {

        this.model = model;
        this.newRatingView = newRatingView;
        this.ratingListView = ratingListView;

        $(this.newRatingView).on('ratingChanged', (event, rating) => this.updateRating(rating));
        $(this.newRatingView).on('ratingAdded', (event, rating) => this.addRating());
        $(this.ratingListView).on('ratingRemoved', (event, ratingId) => {
            this.removeRating(ratingId)
        });

        $(this.model).on('change', () => this.renderViews());

        this.renderViews();
    }

    renderViews() {
        this.newRatingView.render(this.model.newRating);
        this.ratingListView.render(this.model.ratings);
    }

    updateRating(rating) {
        this.model.newRating.name = rating.name;
        this.model.newRating.grade = rating.grade;
    }

    addRating() {
        this.model.addRating();
    }

    removeRating(ratingId) {
        this.model.removeRating(ratingId);
    }

}
