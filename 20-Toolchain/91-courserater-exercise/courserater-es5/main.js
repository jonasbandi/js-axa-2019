courserater.ratingController.init();
courserater.statisticsController.init();


// Layout Logic
enableTab(location.hash);

function enableTab(hash) {
    if (hash === '') hash = '#main';
    $('.container section').hide();
    $(hash).show();

    $('nav [href]').parent().removeClass('active');
    $('[href="' + hash + '"]').parent().addClass('active');
}

window.addEventListener('hashchange', function() {
    enableTab(location.hash);
});