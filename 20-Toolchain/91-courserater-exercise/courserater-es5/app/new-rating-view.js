(function(global) {
    'use strict';

    function NewRatingView() {

        var self = this;

        var nameInput = $('#name');
        var gradeInput = $('#grade');
        var addBtn = $('#addBtn');

        self.render =function(rating){
            nameInput.val(rating.name);
            gradeInput.val(rating.grade);
        };

        nameInput.on('blur', function () {
           changeRating()
        });
        gradeInput.on('blur', function () {
            changeRating()
        });
        addBtn.on('click', function () {
            addRating()
        });


        function changeRating(){
            $(self).trigger({type: 'ratingChanged', rating: {name: nameInput.val(), grade: parseInt(gradeInput.val())}});
        }

        function addRating() {
            $(self).trigger({type: 'ratingAdded'});
        }
    }

    global.courserater = global.courserater || {};
    global.courserater.NewRatingView = NewRatingView;

})(window);
