// Run the script with babel-node
// Make sure you have babel configured for function bind (i.e. presets contains stage-0)

import { take } from "trine/iterable/take";
import { to } from "trine/iterable/to";

// -------------------------------------

function sayHello(peer){ console.log('Hello to ' + peer.name + ' from ' + this.name)}

let universe = {name: 'universe'};
let world = {name: 'world'};

world::sayHello(universe);
universe::sayHello(world);

// -------------------------------------

let evenFilter = function() { return this % 2 === 0; };
let array = [1, 2, 3, 4];

let filtered = array::take(evenFilter);

console.log(filtered::to(Array));
