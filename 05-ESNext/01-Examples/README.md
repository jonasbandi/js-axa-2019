For the corresponding TypeScript examples see in the TypeScript directory.

Install the dependencies:

    yarn install


## Decorator Example:

Run with babel-node:

    yarn run decorator
    
    
    
## Async Example

The example can be run in the latest versions of modern browsers.

Run with babel-node:
    
    yarn run async
    


## Bind Example

Run with babel-node:
    
    yarn run bind