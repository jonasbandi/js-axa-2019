module.exports = {
	"env": {
		browser: true
	},
    "extends": "eslint:recommended",
    "rules":{
        "eqeqeq": ["error", "always"],
        "no-console": "off"
    }
};