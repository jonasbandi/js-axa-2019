## Exercise 5: ES2015

### Exercise 5.1
Try to solve the problem from exercise 3 with ES2015 constructs (`let` and `for-of`-loop & arrow function). The solution should be much more intuitive than with ES5.


### Exercise 5.2
Inspect the example `04-ES2015/92-Spaghetti`. It is a variation of the "spaghetti problem" in exercise 4.

Try to solve the problem with ES2015 modules. The solution should be much more intuitive than with ES5.  
Have a look at the example using ES2015 modules here: `04-ES2015/01-LanguageFeatures/10-Static-Modules`.

Use a modern browser that supports ES2015 modules natively.



## Exercise 6: TypeScript with 3rd Party Libraries

In the example `10-TypeScript/91-3rdParty`:

- Run the example:

		npm install		
		npm start
	
- A browser should open and you see an error at runtime.
- Rename the file `app.js` to `app.ts` ... do you see any changes?
- Install the type definitions for `lodash`:

		npm install @types/lodash
		
- What did change? You should now get a compiler error ...



## Exercise 7: AJAX with jQuery & Callbacks
Inspect the example `08-Async/20-Promises/90-Exercise-jQuery-callbacks`.

Start the Node server on the commandline:

	cd server
	npm install
	npm start

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

	http://localhost:3456/word/0
	
The last parameter can be varied between 0 and 8.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use jQuery with callbacks to implement the logic.



## Exercise 8: AJAX with Promises
Inspect the example `08-Async/20-Promises/91-Exercise-promises`.

Important: Do not use Internet Explorer to run this example since it does not implement native promises!

Start the Node server on the commandline:

	cd _server
	npm install
	npm start

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

	http://localhost:3456/word/0
	
The last parameter can be varied between 0 and 8.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use axios and Promises to implement the logic.


### Exercise 8.1
Rewrite the solution from exercise 10 using `async` and `await`.



