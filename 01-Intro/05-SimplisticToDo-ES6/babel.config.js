/* eslint-env node */

// NOTE: This config file is not used currently, since we have a .babelrc
// Jest 23.5 has not yet switched to use babel.config.js, so we are stuck with .babelrc

// var env = process.env.NODE_ENV;
//
// const presets = [
//     ['@babel/env', {
//         targets: {
//             edge: '17',
//             firefox: '60',
//             chrome: '67',
//             safari: '11.1'
//         },
//         'modules': env === 'test' ? true : false
//     }]
// ];
//
// const plugins = [
//   '@babel/plugin-proposal-class-properties'
// ];
//
// module.exports = { presets, plugins };