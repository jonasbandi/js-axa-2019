import '../css/base.css';
import '../css/index.css';
import $ from 'jquery';
import 'fittextjs';

export function layout() {
    window.$ = $;
    $('#title').fitText(0.7);
}