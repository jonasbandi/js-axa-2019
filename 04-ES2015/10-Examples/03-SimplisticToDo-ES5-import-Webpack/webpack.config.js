var path = require("path");

var config = {
	devtool: 'source-map',
	entry:'./app/src/app.js',
	output: {
		path: path.resolve(__dirname, "./app"),
		filename: 'dist/bundle.js'
	},
	module: {
		loaders: [
			{test: /\.css$/, loader: 'style-loader!css-loader'},
			{test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader"}
		]
	},
	devServer: {
        contentBase: "./app",
	}
	
};

module.exports = config;