import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/main.css';

import {controller} from './controller';

controller.init();
