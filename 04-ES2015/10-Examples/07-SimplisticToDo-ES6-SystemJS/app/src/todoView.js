import $ from 'jquery';

export let todoView = {
    init() {
        this.todoInputElem = $('#input');
        this.addBtn = $('#addBtn');

        let $todoView = $(this); // use the event infrastructure of jQuery
        this.todoInputElem.on('blur', () => $todoView.trigger('todo-entered', this.todoInputElem.val())); // Note ES6 lexical scoping!

        this.addBtn.on('click', () => $todoView.trigger('todo-added'));
    },
    render(todo) {
        this.todoInputElem.val(todo.text);
    }
};