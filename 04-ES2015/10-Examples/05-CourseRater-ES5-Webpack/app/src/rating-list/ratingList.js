module.exports = function () {
    return {
        restrict: 'E',
        template: require('./rating-list.html'),
        scope: {},
        controller: Controller,
        controllerAs: 'vm',
        bindToController: {ratings: '=', remove: '&'}
    }
};

function Controller() {
    var vm = this;

    vm.removeRating = function (item) {
        vm.remove()(item);
    }
}


