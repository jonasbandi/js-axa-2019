// A generator function is a function that creates a generator instance
function* createRange(start, end, step) {
    while (start < end) {
        yield start; // generator functions are pausable
        start += step;
    }
}

// A generator instance is an object
console.log('Generator:' + createRange(0, 10, 2));

// A generator instance is iterable
console.log('Generator first value:' + createRange(0, 10, 2).next().value);
for (let i of createRange(0, 10, 2)) {
    console.log(i);
}

/* --------------------------------*/

// function* createGreeter(){
//     console.log(`You called 'next()'`);
//     yield "hello";
// }

// // Generators return objects that hold the yielded value
// let greeter = createGreeter();
// console.log(greeter);
// let next = greeter.next();
// console.log(next);
// let done = greeter.next();
// console.log(done);

// for (let v of createGreeter()) {
//     console.log(v);
// }

/* --------------------------------*/
// The generator function and the generator instance can pass messages
// The control flow switches between the generator function and the client using the generaor instance

// function* createHero() {
//     let count = 0;
//     let hero = 'Superman';
// 	hero = yield `${++count} ${hero}` // the new value of hero is passed as argument to .next()
// 	console.log(hero);
//     hero = yield `${++count} ${hero}` // the new value of hero is passed as argument to .next()
// 	console.log(hero);
//     return `${++count} ${hero}`;
// }

// const hello = createHero()
// console.log(hello.next())
// console.log(hello.next('Batman'))
// console.log(hello.next('Spiderman'))



