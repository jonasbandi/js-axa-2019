lesson('about the spread operator', () => {

    learn('how to spread an array into separate arguments', () => {

        function takeThreeArguments(x, y, z) {
            return x + y + z;
        }

        const array = [1, 2, 3];
        expect(takeThreeArguments(...array)).toBe(FILL_ME_IN);
    });

    learn('how to clone and create a new array with spread', () => {

        const array = [1, 2, 3];

        const clonedArray = [...array];

        expect(array === clonedArray).toBe(FILL_ME_IN); // its a new array

        // compare the content of both arrays:
        let isDifferent = false;
        for (let i = 0; i < clonedArray.length; i++) {
            if (array[i] !== clonedArray[i]) isDifferent = true;
        }
        expect(isDifferent).toBe(FILL_ME_IN);

        const newValue = 4;
        const newArray = [...array, newValue];

        // compare the content of both arrays:
        isDifferent = false;
        for (let i = 0; i < array.length; i++) {
            if (array[i] !== newArray[i]) isDifferent = true;
        }
        expect(isDifferent).toBe(FILL_ME_IN);
        expect(newArray[3]).toBe(FILL_ME_IN);
    });

    /// NOTE:
    /// The code below only works if you transpile with Babel and the appropriate babel-plugin
    /// In the test-setup this is the case if you select the 'transpile' checkbox


    // learn("how to clone and create a new object with object spread in ES.next", () => {
    //
    //     //     // Object Rest/Spread Properties ist currently a stage 3 proposal for ECMAScript
    //     //     // https://github.com/sebmarkbage/ecmascript-rest-spread
    //     //     // Babel however allows you to use that feature already
    //
    //     const obj = {three: 3, four: 4 , five: 5, six: 6};
    //
    //     const clonedObj = {...obj};
    //     expect(obj === clonedObj).toBe(FILL_ME_IN); // its a new object
    //
    //     // compare the content of both objects:
    //     let isDifferent = false, i = 0;
    //     for (let prop in obj) {
    //         if (clonedObj[prop] !== obj[prop]) isDifferent = true;
    //     }
    //     expect(isDifferent).toBe(FILL_ME_IN);
    //
    //     let eight = 8;
    //     const newObject = {...obj, seven: 7, eight};
    //     for (let prop in obj) {
    //         if (newObject[prop] !== obj[prop]) isDifferent = true;
    //     }
    //     expect(isDifferent).toBe(FILL_ME_IN);
    //     expect(newObject.seven).toBe(FILL_ME_IN);
    //     expect(newObject.eight).toBe(FILL_ME_IN);
    //
    // });
});

