
Calculator = function (eq) {
    // private members
    var eqCtl = document.getElementById(eq);

    return {
        // public members
        add: function(x,y) {
            var val = x + y;
            eqCtl.innerHTML = val;
        }
    }
};

