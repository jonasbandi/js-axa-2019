var Calculator = function(eq) {
    // state
    this.eqCtl = document.getElementById(eq);
};

Calculator.prototype = function() {

    // Private method
    var internalAdd = function(x,y){
        var val = x + y;
        this.eqCtl.innerHTML = val;
    };

    // Public method
    var add = function(x,y) {
        var val = x + y;
        this.eqCtl.innerHTML = val;

        // Private Methods loose the 'this' context! This shows the limits of OO in JavaScript...
//        internalAdd(x,y);
//        internalAdd.call(this, x,y);
    };

    // public members
    return {
        add: add
    };
}();
