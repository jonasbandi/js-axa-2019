
var calculator = (function (eq) {
    // private members
    var eqCtl = document.getElementById(eq);
    var add = function(x,y) {
        var val = x + y;
        eqCtl.innerHTML = val;
    }

    return {
        // public members
        add: add
    }
})('eq');

