var cashtml5 = cashtml5 || {};

cashtml5.Calculator = function (eq) {
    this.eqCtl = document.getElementById(eq);
};

cashtml5.Calculator.prototype = {
    add: function (x, y) {
        var val = x + y;
        this.eqCtl.innerHTML = val;
    }
};
