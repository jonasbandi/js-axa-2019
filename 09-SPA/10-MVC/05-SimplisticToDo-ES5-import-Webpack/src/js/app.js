import * as $ from 'jquery';
import {controller} from './controller';

controller.init();



// SPA basics: Prevent form submit
$('form').on('submit', function (e) { e.preventDefault() })

