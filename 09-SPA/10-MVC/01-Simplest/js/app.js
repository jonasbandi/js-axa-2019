(function () {
    'use strict';

    /* ======= Model ======= */
    function createModel() {

        var currentFramework = null;
        var frameworks = [
            {
                likeCount: 0,
                name: 'Backbone',
                imgSrc: 'img/backbone.jpg',
                url: 'http://backbonejs.org/'
            },
            {
                likeCount: 0,
                name: 'Marionette',
                imgSrc: 'img/marionette.png',
                url: 'http://marionette.com/'
            },
            {
                likeCount: 0,
                name: 'EmberJS',
                imgSrc: 'img/ember.jpg',
                url: 'http://emberjs.com/'
            },
            {
                likeCount: 0,
                name: 'Knockout',
                imgSrc: 'img/knockout.jpg',
                url: 'http://knockoutjs.com/'
            },
            {
                likeCount: 0,
                name: 'AngularJS',
                imgSrc: 'img/angular.jpg',
                url: 'https://angularjs.org/'
            },
            {
                likeCount: 0,
                name: 'React',
                imgSrc: 'img/react.png',
                url: 'http://facebook.github.io/react/'
            }

        ];

        var model = {
            getFrameworkList: function () { return frameworks; },
            getCurrentFramework: function () { return currentFramework; },
            setCurrentFramework: function (framework) { currentFramework = framework; model.trigger('change') },
            likeCurrentFramework: function () { currentFramework.likeCount++; model.trigger('change') }
        }
        MicroEvent.mixin(model);

        return model;
    };


    /* ======= View ======= */

    MicroEvent.mixin(FrameworkView);
    function FrameworkView() {

        var self = this;

        var frameworkNameElem = document.getElementById('framework-name');
        var logoImageElem = document.getElementById('logo-img');
        var likeCountElem = document.getElementById('like-count');
        var likeButtonElem = document.getElementById('like-button');

        likeButtonElem.addEventListener('click', function () {
            self.trigger('like');
        });

        self.render = function (currentFramework) {
            likeCountElem.textContent = currentFramework.likeCount;
            frameworkNameElem.textContent = currentFramework.name;
            logoImageElem.src = currentFramework.imgSrc;
        }
    }

    MicroEvent.mixin(FrameworkListView);
    function FrameworkListView() {

        var self = this;

        var frameworkListElem = document.getElementById('framework-list');

        self.render = function (frameworks) {
            var framework, elem, i;

            // Clear the list
            frameworkListElem.innerHTML = '';

            for (i = 0; i < frameworks.length; i++) {
                framework = frameworks[i];

                elem = document.createElement('li');
                elem.textContent = framework.name;

                elem.addEventListener('click', (function (framework) {
                    // Capture context in a closure
                    return function () {
                        self.trigger('item-selected', framework);
                    };
                })(framework));

                frameworkListElem.appendChild(elem);
            }
        }
    }


    /* ======= Controller ======= */

    var controller = {

        init: function () {

            var model = createModel();

            // set initial framework
            model.setCurrentFramework(model.getFrameworkList()[0]);

            var frameworkListView = new FrameworkListView();
            var frameworkView = new FrameworkView();

            frameworkListView.bind('item-selected', setCurrentFramework);
            frameworkView.bind('like', incrementLikes);

            frameworkListView.render(model.getFrameworkList());
            frameworkView.render(model.getCurrentFramework());

            model.bind('change', updateUi);

            function setCurrentFramework(framework) {
                model.setCurrentFramework(framework);
            }

            function incrementLikes() {
                model.likeCurrentFramework();
            }

            function updateUi() {
                // currently this re-renders the whole UI ... of course this should be optimized 
                frameworkListView.render(model.getFrameworkList());
                frameworkView.render(model.getCurrentFramework());
            }
        }
    };

    // make it go!
    controller.init();
})();