// See: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

const getBtn = document.getElementById('get');
const postBtn = document.getElementById('post');
const idInput = document.getElementById('id');
const putBtn = document.getElementById('put');
const deleteBtn = document.getElementById('delete');

getBtn.addEventListener('click', getData);
postBtn.addEventListener('click', postData);
putBtn.addEventListener('click', putData);
deleteBtn.addEventListener('click', deleteData);


function getData() {

    fetch('http://localhost:3001/comments2')
        .then(response => response.json())
        .then(data => console.log('SUCCESS', data))
        .catch(error => console.log('ERROR:', error));
}

function postData() {

    const options = {
        method: 'POST',
        headers: new Headers({"Content-Type": "application/json"}),
        body: JSON.stringify({text: 'test - ' + new Date()})

    };

    fetch('http://localhost:3001/comments', options)
        .then(data => console.log('POSTED!'));

}

function putData() {

    const id = idInput.value;

    const options = {
        method: 'PUT',
        headers: new Headers({"Content-Type": "application/json"}),
        body: JSON.stringify({text: 'test - ' + new Date()})

    };

    fetch('http://localhost:3001/comments/' + id, options)
        .then(data => console.log('PUT!'));

}

function deleteData() {

    const id = idInput.value;

    const options = {
        method: 'DELETE',
    };

    fetch('http://localhost:3001/comments/' + id, options)
        .then(data => console.log('PUT!'));

}


// Note: The reality is more complex:
/*
fetch('http://localhost:3001/comments2')
    .then(response => response.ok ? Promise.resolve(response) : Promise.reject(response.statusText))
    .then(response => response.json())
    .then(data => console.log('SUCCESS', data))
    .catch(error => console.log('ERROR:', error));
*/